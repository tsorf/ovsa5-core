package ru.bisv.api.contract

enum class ScVideoProtocol {

    RTMP,

    RTSP,

    HLS,

    HDS
}