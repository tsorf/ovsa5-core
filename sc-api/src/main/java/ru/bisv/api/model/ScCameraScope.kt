package ru.bisv.api.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class ScCameraScope constructor(
    val constraint: Constraint,
    private val tagUuid: String?
) : Parcelable {

    val requiredTagUuid: String
        get() {
            requireNotNull(tagUuid)
            return tagUuid
        }

    enum class Constraint {
        NONE,
        FAVORITES,
        TAG
    }

    companion object {

        fun all(): ScCameraScope {
            return ScCameraScope(Constraint.NONE, null)
        }

        fun favorites(): ScCameraScope {
            return ScCameraScope(Constraint.FAVORITES, null)
        }

        fun tag(tagUuid: String): ScCameraScope {
            return ScCameraScope(Constraint.TAG, tagUuid)
        }
    }
}