package ru.bisv.api.model

data class ScTag(
    val uuid: String,
    val id: String,
    val name: String,
    val iconUrl: String,
    val hash: String,
    val sort: Int,
    val createdAt: Long
)
