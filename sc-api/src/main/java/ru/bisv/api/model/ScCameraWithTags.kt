package ru.bisv.api.model

data class ScCameraWithTags(
    val camera: ScCamera,
    val tags: List<ScTag>
)
