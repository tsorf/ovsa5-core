package ru.bisv.api.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScCamera(
    val uuid: String,
    val streamId: String,
    val text: String,
    val name: String,
    val addedAt: String,
    val thumbnail: Thumbnail,
    val search: String?,
    val sdStreamUrl: String?,
    val hdStreamUrl: String?,
    val isAlive: Boolean,
    val isFavorite: Boolean,
    val tags: List<String>,
    val createdAt: Long
) : Parcelable {

    @Parcelize
    data class Thumbnail(
        val url: String,
        val urlLarge: String
    ) : Parcelable
}
