package ru.bisv.api.model

data class ScCameraWithTagsIds(
    val camera: ScCamera,
    val ids: List<String>
)
