package ru.bisv.api.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScCameraMetadata(
    val title: CharSequence
) : Parcelable