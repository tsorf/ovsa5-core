package ru.bisv.api.model

data class ScCamerasData(
    val cameras: List<ScCameraWithTagsIds>,
    val tags: List<ScTag>
)