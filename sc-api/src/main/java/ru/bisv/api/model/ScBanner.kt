package ru.bisv.api.model

data class ScBanner(
    val uuid: String,
    val url: String,
    val href: String,
    val timeout: Int
)