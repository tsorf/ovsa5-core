package ru.bisv.db.repo

interface ScRepo {

    fun db(): Database

    interface Database {

        fun <Result>transaction(actions: (Database) -> Result) : Result
    }
}