package ru.bisv.db.domain

import ru.bisv.api.model.ScCamera

interface FavoritesRepo {

    fun findFavoriteCameras(): List<ScCamera>

    fun addToFavorites(camera: ScCamera)

    fun deleteFromFavorites(camera: ScCamera)

    fun clearFavorites()
}