package ru.bisv.db.domain

import ru.bisv.api.model.ScTag

interface TagsRepo {

    fun findAll(): List<ScTag>
}