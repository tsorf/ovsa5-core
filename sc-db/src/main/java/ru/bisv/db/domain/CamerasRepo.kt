package ru.bisv.db.domain

import ru.bisv.api.model.*
import ru.bisv.db.repo.ScRepo

interface CamerasRepo : ScRepo {

    fun checkIfLoaded(): Boolean

    fun findAll(): List<ScCamera>

    fun findFavorites(): List<ScCamera>

    fun findByStreamId(streamId: String): ScCameraWithTags?

    fun findByUuid(cameraUuid: String): ScCameraWithTags?

    fun updateCamerasAndTags(data: ScCamerasData)

    fun findByTag(tagUuid: String): List<ScCamera>

    fun update(camera: ScCamera)

    fun delete(cameraUuid: String)

    fun search(query: String): List<ScCameraWithTags>
}