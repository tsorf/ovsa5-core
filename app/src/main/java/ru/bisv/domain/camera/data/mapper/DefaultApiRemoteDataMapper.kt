package ru.bisv.domain.camera.data.mapper

import ru.bisv.api.model.ScCameraWithTagsIds
import ru.bisv.api.model.ScCamerasData
import ru.bisv.api.model.ScTag
import ru.bisv.db.room.mapper.Mapper
import ru.bisv.networking.dto.RemoteCamera
import ru.bisv.networking.dto.RemoteTag

interface ApiRemoteDataMapper {

    fun convert(remoteCameras: List<RemoteCamera>, remoteTags: List<RemoteTag>): ScCamerasData
}

class DefaultApiRemoteDataMapper(
    private val cameraMapper: Mapper<RemoteCamera, ScCameraWithTagsIds>,
    private val tagMapper: Mapper<RemoteTag, ScTag>
) : ApiRemoteDataMapper {

    override fun convert(
        remoteCameras: List<RemoteCamera>,
        remoteTags: List<RemoteTag>
    ): ScCamerasData {
        val cameras = remoteCameras.map(cameraMapper::convert)
        val tags = remoteTags.map(tagMapper::convert)
        return ScCamerasData(cameras, tags)
    }
}