package ru.bisv.domain.camera.data.mapper

import ru.bisv.api.model.ScTag
import ru.bisv.db.room.dao.generateUuid
import ru.bisv.db.room.dao.timeStamp
import ru.bisv.db.room.mapper.Mapper
import ru.bisv.networking.dto.RemoteTag
import ru.bisv.ui.common.images.ImageResolver

class ApiRemoteTagMapper(imageResolver: ImageResolver) : Mapper<RemoteTag, ScTag> {

    override fun convert(source: RemoteTag): ScTag {
        return ScTag(
            generateUuid(),
            source.id,
            source.name,
            source.iconUrl,
            source.hash,
            source.sort,
            timeStamp()
        )
    }
}