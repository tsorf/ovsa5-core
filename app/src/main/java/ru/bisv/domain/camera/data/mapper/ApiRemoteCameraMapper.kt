package ru.bisv.domain.camera.data.mapper

import ru.bisv.api.model.ScCamera
import ru.bisv.db.room.dao.generateUuid
import ru.bisv.db.room.dao.timeStamp
import ru.bisv.db.room.mapper.Mapper
import ru.bisv.networking.dto.RemoteCamera

class ApiRemoteCameraMapper : Mapper<RemoteCamera, ScCamera> {

    override fun convert(source: RemoteCamera): ScCamera {
        return ScCamera(
            generateUuid(),
            source.streamId,
            source.text,
            source.name,
            source.createdAt,
            ScCamera.Thumbnail(
                source.thumbnail,
                source.thumbnailLarge
            ),
            source.search,
            source.sdStreamUrl,
            source.hdStreamUrl,
            source.isAlive,
            false,
            source.tags,
            timeStamp()
        )
    }
}