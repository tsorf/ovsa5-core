package ru.bisv.domain.camera.data.mapper

import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraWithTagsIds
import ru.bisv.db.room.dao.generateUuid
import ru.bisv.db.room.dao.timeStamp
import ru.bisv.db.room.mapper.Mapper
import ru.bisv.networking.dto.RemoteCamera

class ApiRemoteCameraWithTagIdsMapper : Mapper<RemoteCamera, ScCameraWithTagsIds> {

    override fun convert(source: RemoteCamera): ScCameraWithTagsIds {
        return ScCameraWithTagsIds(
            ScCamera(
                generateUuid(),
                source.streamId,
                source.text,
                source.name,
                source.createdAt,
                ScCamera.Thumbnail(
                    source.thumbnail,
                    source.thumbnailLarge
                ),
                source.search,
                source.sdStreamUrl,
                source.hdStreamUrl,
                source.isAlive,
                false,
                source.tags,
                timeStamp()
            ),
            source.tags
        )
    }
}