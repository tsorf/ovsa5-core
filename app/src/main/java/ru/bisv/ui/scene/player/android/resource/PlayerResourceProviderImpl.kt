package ru.bisv.ui.scene.player.android.resource

import android.content.Context
import ru.bisv.R
import ru.bisv.ui.scene.player.viewmodel.PlayerResourceProvider
import javax.inject.Inject

class PlayerResourceProviderImpl @Inject constructor(
    private val context: Context
) : PlayerResourceProvider {

    override fun provideCameraNotFoundErrorMessage(): CharSequence {
        return context.getString(R.string.camera_not_found)

    }

    override fun wrongCameraScope(): CharSequence {
        return context.getString(R.string.wrong_camera_scope)
    }
}