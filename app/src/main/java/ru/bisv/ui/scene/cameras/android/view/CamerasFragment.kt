package ru.bisv.ui.scene.cameras.android.view

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.transition.TransitionInflater
import ru.bisv.R
import ru.bisv.api.model.ScCameraMetadata
import ru.bisv.api.model.ScCameraScope
import ru.bisv.databinding.FragmentCamerasBinding
import ru.bisv.ui.scene.cameras.CamerasContract
import ru.bisv.ui.scene.cameras.android.adapter.CameraAdapterItem
import ru.bisv.ui.scene.cameras.android.adapter.FeaturedCameraAdapterItem
import ru.bisv.ui.scene.player.android.dto.CameraInfoWithSharedElement
import ru.bisv.ui.scene.player.android.dto.CameraWithPreviewView
import ru.frosteye.ovsa.adapter.AdapterEvent
import ru.frosteye.ovsa.adapter.multiTypeAdapter
import ru.frosteye.ovsa.commons.dagger.inject.Injectable
import ru.frosteye.ovsa.commons.extensions.*
import ru.frosteye.ovsa.commons.messages.defaultMessenger
import ru.frosteye.ovsa.commons.rx.disposable.DisposableTrashCan
import ru.frosteye.ovsa.commons.rx.disposable.SimpleDisposableTrashCan
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.binding
import ru.frosteye.ovsa.mvvm.view.ControlsGroup
import javax.inject.Inject

class CamerasFragment : Fragment(R.layout.fragment_cameras),
    CamerasContract.View,
    BindingView<FragmentCamerasBinding>,
    DisposableTrashCan by SimpleDisposableTrashCan(),
    Injectable {

    @Inject
    override lateinit var viewModel: CamerasContract.ViewModel

    @Inject
    override lateinit var router: CamerasContract.Router

    override val messenger by defaultMessenger()

    override val boundView by binding { requireView() }

    private val adapter = multiTypeAdapter()

    private val scope by requiredArgument<ScCameraScope>()
    private val meta by requiredArgument<ScCameraMetadata>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.slide_in)
        exitTransition = inflater.inflateTransition(R.transition.go_out)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val layoutManager = GridLayoutManager(context, 2).also {
            it.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position % FEATURED_RATE == 0) 2 else 1
                }
            }
        }
        boundView.apply {
            list.also {
                it.adapter = adapter
                it.layoutManager = layoutManager
            }
            swipe.setOnRefreshListener {
                boundView.swipe.isRefreshing = false
            }
            toolbar.setNavigationOnClickListener {
                onBackPressed()
            }
            setTitle(meta.title, null)
        }

        +adapter.adapterEvents.subscribe {
            if (it is AdapterEvent.Select) {
                it.model.safeCast<CameraWithPreviewView>()?.let { pack ->
                    router.navigateToCamera(CameraInfoWithSharedElement(
                        pack.camera, scope, pack.view
                    ))
                }
            }
        }

        observe(viewModel.state) {
            when (it) {
                is CamerasContract.ViewState.Ready -> {
                    setTitle(meta.title, it.cameras.size)
                    showItems(it.cameras)
                }
            }
        }

        viewModel.requestCameras(scope)

    }

    private fun setTitle(title: CharSequence, count: Int?) {
        val titleText = title.toString().capitalize()
        if (count != null) {
            val countString = " (${count})"
            val result = "${titleText}${countString}"
            boundView.collapsingToolbar.title = SpannableStringBuilder(result).apply {
                setSpan(
                    ForegroundColorSpan(Color.LTGRAY), title.length, result.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                setSpan(
                    RelativeSizeSpan(0.7f), title.length, result.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        } else {
            boundView.collapsingToolbar.title = title
        }
    }

    private fun showItems(items: List<CamerasContract.CameraGridItem>) {
        adapter.apply {
            clear()
            val cameras = items
                .asSequence()
                .filterIsInstance<CamerasContract.CameraGridItem.Camera>()
                .mapIndexed { index, item ->
                    if (index % FEATURED_RATE == 0) {
                        FeaturedCameraAdapterItem(
                            item
                        )
                    } else {
                        CameraAdapterItem(
                            item
                        )
                    }
                }.toList()
            addItems(cameras)
            boundView.list.scheduleLayoutAnimation()
        }
    }


    override fun onHiddenChanged(hidden: Boolean) {
        if (hidden) {
            clearDecorViewEffects()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setSystemUiFlags(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                setStatusBarColor(resources.getColor(R.color.white))
            }
        }
    }

    override fun enableControls(enabled: Boolean, controlsGroup: ControlsGroup) {
        if (enabled) {
            boundView.swipe.isRefreshing = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeTrashCan()
    }

    companion object {

        const val FEATURED_RATE = 5
    }
}