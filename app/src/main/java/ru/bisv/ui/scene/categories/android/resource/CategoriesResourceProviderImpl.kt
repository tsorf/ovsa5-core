package ru.bisv.ui.scene.categories.android.resource

import android.content.Context
import ru.bisv.R
import ru.bisv.ui.common.images.ImageResolver
import ru.bisv.ui.scene.categories.viewmodel.CategoriesResourceProvider
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview
import javax.inject.Inject

class CategoriesResourceProviderImpl @Inject constructor(
    private val context: Context,
    private val imageResolver: ImageResolver
) : CategoriesResourceProvider {

    override fun provideFavoritesDescription(): Pair<CharSequence, ImagePreview?> {
        val path = imageResolver.resolve("/img/tags/favorites.png")
        return context.getString(R.string.favorites) to path
    }

    override fun provideAllCamerasDescription(): Pair<CharSequence, ImagePreview?> {
        val path = imageResolver.resolve("/img/tags/Vse.png")
        return context.getString(R.string.all_cameras) to path
    }
}