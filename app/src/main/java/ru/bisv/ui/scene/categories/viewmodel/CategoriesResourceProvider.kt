package ru.bisv.ui.scene.categories.viewmodel

import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

interface CategoriesResourceProvider {

    fun provideFavoritesDescription(): Pair<CharSequence, ImagePreview?>

    fun provideAllCamerasDescription(): Pair<CharSequence, ImagePreview?>
}