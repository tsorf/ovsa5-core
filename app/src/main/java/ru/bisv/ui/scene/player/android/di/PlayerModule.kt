package ru.bisv.ui.scene.player.android.di

import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.intrtl.lib2.di.domain.presenter.scope.UiScope
import dagger.Module
import dagger.Provides
import ru.bisv.ui.scene.player.PlayerContract
import ru.bisv.ui.scene.player.android.router.PlayerRouter
import ru.bisv.ui.scene.player.android.view.PlayerActivity
import ru.bisv.ui.scene.player.viewmodel.PlayerViewModel

@Module
internal class PlayerModule {

    @Provides
    @UiScope
    fun viewModel(
        activity: PlayerActivity,
        factory: ViewModelProvider.Factory
    ): PlayerContract.ViewModel {
        return activity.viewModels<PlayerViewModel> { factory }.value
    }

    @Provides
    @UiScope
    fun router(activity: PlayerActivity): PlayerContract.Router {
        return PlayerRouter(activity)
    }
}