package ru.bisv.ui.scene.cameras.android.router

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.bisv.ui.scene.cameras.CamerasContract
import ru.bisv.ui.scene.player.PlayerContract
import ru.bisv.ui.scene.player.android.dto.CameraInfoWithSharedElement
import ru.bisv.ui.scene.player.android.view.PlayerActivity
import ru.frosteye.ovsa.commons.extensions.put
import ru.frosteye.ovsa.commons.extensions.putExtra
import ru.frosteye.ovsa.commons.router.DefaultActivityRouter

class CamerasRouter(
    activity: AppCompatActivity
) : DefaultActivityRouter(activity), CamerasContract.Router {

    override fun navigateToCamera(cameraInfo: CamerasContract.CameraInfo) {
        if (cameraInfo is CameraInfoWithSharedElement) {
            val intent = Intent(activity, PlayerActivity::class.java).also {
                it.putExtra(PlayerContract.Argument(cameraInfo.camera, cameraInfo.scope))
            }
            val options = ActivityOptions
                .makeSceneTransitionAnimation(
                    activity,
                    cameraInfo.sharedView,
                    cameraInfo.camera.uuid
                )
            activity.startActivity(intent, options.toBundle())
        } else {
            navigateToActivity(PlayerActivity::class.java, Bundle().apply {
                put(PlayerContract.Argument(cameraInfo.camera, cameraInfo.scope))
            })
        }
    }


}