package ru.bisv.ui.scene.categories.usecase

import io.reactivex.rxjava3.core.Observable
import ru.bisv.api.model.ScTag
import ru.bisv.db.domain.CamerasRepo
import ru.bisv.db.domain.TagsRepo
import ru.bisv.domain.camera.data.mapper.ApiRemoteDataMapper
import ru.bisv.networking.api.CamerasApi
import ru.bisv.ui.common.images.ImageResolver
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.frosteye.ovsa.commons.domain.usecase.UseCase
import ru.frosteye.ovsa.commons.rx.schedule.subscribeOnIo
import javax.inject.Inject

class LoadCamerasAndTagsUseCase @Inject constructor(
    private val camerasRepo: CamerasRepo,
    private val camerasApi: CamerasApi,
    private val tagsRepo: TagsRepo,
    private val imageResolver: ImageResolver,
    private val dataMapper: ApiRemoteDataMapper
) : UseCase<Void?, LoadCamerasAndTagsUseCase.Result> {

    override fun build(param: Void?): Observable<Result> {
        val cachingObservable = camerasApi.getCameras()
            .subscribeOnIo()
            .toObservable()
            .map {
                val data = dataMapper.convert(it.cameras, it.tags)
                camerasRepo.db().transaction {
                    camerasRepo.updateCamerasAndTags(data)
                }
                createResult(data.tags)
            }
        val localObservable = Observable.fromCallable {
            camerasRepo.db().transaction {
                tagsRepo.findAll()
            }
        }
            .subscribeOnIo()
            .filter {
                it.isNotEmpty()
            }
            .map(this::createResult)
        return Observable.concat(localObservable, cachingObservable)
    }

    private fun createResult(items: List<ScTag>): Result {
        return items.asSequence().filter {
            it.hash != "all"
        }.map {
            CategoriesContract.ScTagWithPreview(it, imageResolver.resolve(it.iconUrl))
        }.toList().let {
            Result(it)
        }
    }

    data class Result(
        val tags: List<CategoriesContract.ScTagWithPreview>
    )
}