package ru.bisv.ui.scene.player.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.bisv.ui.scene.player.PlayerContract
import ru.bisv.ui.scene.player.usecase.PreparePlayerCameraSetUseCase
import ru.frosteye.ovsa.mvvm.viewmodel.AbstractViewModel
import javax.inject.Inject

class PlayerViewModel @Inject constructor(
    private val prepareCamerasUseCase: PreparePlayerCameraSetUseCase
) : AbstractViewModel(), PlayerContract.ViewModel {

    private val _state: MutableLiveData<PlayerContract.ViewState> = MutableLiveData()
    override val state: LiveData<PlayerContract.ViewState> = _state

    private lateinit var argument: PlayerContract.Argument

    override fun initialize(argument: PlayerContract.Argument) {
        this.argument = argument
        postControlsState(false)
        +prepareCamerasUseCase.build(PreparePlayerCameraSetUseCase.Param(
            argument.selectedCamera.uuid, argument.scope
        )).subscribe({
            postControlsState(true)
            _state.value = it
        }, {
            postControlsState(true)
            postError(it.message, error = it)
        })
    }
}