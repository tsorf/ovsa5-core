package ru.bisv.ui.scene.home.android.di

import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.intrtl.lib2.di.domain.presenter.scope.UiScope
import dagger.Module
import dagger.Provides
import ru.bisv.ui.scene.home.HomeContract
import ru.bisv.ui.scene.home.android.router.HomeRouter
import ru.bisv.ui.scene.home.android.view.HomeActivity
import ru.bisv.ui.scene.home.viewmodel.HomeViewModel

@Module
internal class HomeModule {

    @Provides
    @UiScope
    fun viewModel(
        activity: HomeActivity,
        factory: ViewModelProvider.Factory
    ): HomeContract.ViewModel {
        return activity.viewModels<HomeViewModel> { factory }.value
    }

    @Provides
    @UiScope
    fun router(activity: HomeActivity): HomeContract.Router {
        return HomeRouter(activity)
    }
}