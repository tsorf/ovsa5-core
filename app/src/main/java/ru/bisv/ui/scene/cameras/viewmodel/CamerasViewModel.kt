package ru.bisv.ui.scene.cameras.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.bisv.api.model.ScCameraScope
import ru.bisv.ui.scene.cameras.usecase.LoadAllCamerasUseCase
import ru.bisv.ui.scene.cameras.usecase.LoadCamerasByTagUseCase
import ru.bisv.ui.scene.cameras.usecase.LoadFavoriteCamerasUseCase
import ru.bisv.ui.scene.cameras.CamerasContract
import ru.frosteye.ovsa.commons.rx.schedule.observeOnUi
import ru.frosteye.ovsa.mvvm.viewmodel.AbstractViewModel
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CamerasViewModel @Inject constructor(
    private val loadFavoriteCamerasUseCase: LoadFavoriteCamerasUseCase,
    private val loadAllCamerasUseCase: LoadAllCamerasUseCase,
    private val loadCamerasByTagUseCase: LoadCamerasByTagUseCase,
    private val resourceProvider: CamerasResourceProvider
) : AbstractViewModel(), CamerasContract.ViewModel {

    private val _state: MutableLiveData<CamerasContract.ViewState> = MutableLiveData()
    override val state: LiveData<CamerasContract.ViewState> = _state


    override fun requestCameras(scope: ScCameraScope) {
        postControlsState(false)
        val source = when (scope.constraint) {
            ScCameraScope.Constraint.FAVORITES -> loadFavoriteCamerasUseCase.build(null)
            ScCameraScope.Constraint.TAG -> {
                loadCamerasByTagUseCase.build(
                    LoadCamerasByTagUseCase.Param(scope.requiredTagUuid)
                )
            }
            ScCameraScope.Constraint.NONE -> loadAllCamerasUseCase.build(null)
        }
        +source.map { list ->
            list.filter {
                it.isAlive
            }
        }.subscribe({ items ->
            postControlsState(true)
            _state.value = CamerasContract.ViewState.Ready(
                items.map {
                    CamerasContract.CameraGridItem.Camera(it)
                }
            )
        }, {
            postControlsState(true)
            postError(it.message)
        })
    }
}