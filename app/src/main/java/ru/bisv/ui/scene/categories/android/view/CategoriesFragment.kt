package ru.bisv.ui.scene.categories.android.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.transition.TransitionInflater
import ru.bisv.R
import ru.bisv.databinding.FragmentCategoriesBinding
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.bisv.ui.scene.categories.android.adapter.CategoriesFooterAdapterItem
import ru.bisv.ui.scene.categories.android.adapter.CategoryAdapterItem
import ru.frosteye.ovsa.adapter.AdapterEvent
import ru.frosteye.ovsa.adapter.multiTypeAdapter
import ru.frosteye.ovsa.commons.dagger.inject.Injectable
import ru.frosteye.ovsa.commons.extensions.observe
import ru.frosteye.ovsa.commons.extensions.safeCast
import ru.frosteye.ovsa.commons.extensions.setBackgroundColor
import ru.frosteye.ovsa.commons.messages.Messenger
import ru.frosteye.ovsa.commons.messages.defaultMessenger
import ru.frosteye.ovsa.commons.rx.disposable.DisposableTrashCan
import ru.frosteye.ovsa.commons.rx.disposable.SimpleDisposableTrashCan
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.binding
import ru.frosteye.ovsa.mvvm.view.ControlsGroup
import javax.inject.Inject

class CategoriesFragment : Fragment(R.layout.fragment_categories),
    CategoriesContract.View,
    DisposableTrashCan by SimpleDisposableTrashCan(),
    BindingView<FragmentCategoriesBinding>,
    Injectable {

    @Inject
    override lateinit var viewModel: CategoriesContract.ViewModel

    @Inject
    override lateinit var router: CategoriesContract.Router

    override val messenger: Messenger by defaultMessenger()

    private val adapter = multiTypeAdapter()

    override val boundView by binding { requireView() }

    private var shouldAnimateGrid = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        observeControlsState(this)
        observeMessenger(this)

        requireActivity().window.setBackgroundColor(Color.WHITE)

        val layoutManager = GridLayoutManager(context, 2).also {
            it.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {

                override fun getSpanSize(position: Int): Int {
                    return if (position == adapter.itemCount - 1) 2 else 1
                }
            }
        }
        boundView.list.also {
            it.adapter = adapter
            it.layoutManager = layoutManager
        }
        boundView.swipe.setOnRefreshListener {
            viewModel.refreshData()
        }
        +adapter.adapterEvents.subscribe {
            when (it) {
                is AdapterEvent.Select -> {
                    it.model.safeCast<CategoriesContract.CategoryItem>()?.let { item ->
                        router.navigateToCameras(item)
                    }
                }
            }
        }
        observe(viewModel.categories) {
            showCategories(it)
        }

        viewModel.refreshData()
    }

    private fun showCategories(categories: List<CategoriesContract.CategoryItem>) {
        adapter.apply {
            clear()
            addItems(categories.map {
                CategoryAdapterItem(it)
            } + CategoriesFooterAdapterItem())
            if (shouldAnimateGrid) {
                shouldAnimateGrid = categories.isEmpty()
                boundView.list.scheduleLayoutAnimation()
            }
        }
    }

    override fun enableControls(enabled: Boolean, controlsGroup: ControlsGroup) {
        if (enabled) {
            boundView.swipe.isRefreshing = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeTrashCan()
    }
}