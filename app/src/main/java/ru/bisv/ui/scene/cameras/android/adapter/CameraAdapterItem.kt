package ru.bisv.ui.scene.cameras.android.adapter

import ru.bisv.R
import ru.bisv.ui.scene.cameras.CamerasContract
import ru.frosteye.ovsa.adapter.AdapterItemImpl

class CameraAdapterItem(model: CamerasContract.CameraGridItem.Camera) :
    AdapterItemImpl<CamerasContract.CameraGridItem.Camera, CameraItemView>(model) {

    override val layoutResourceId: Int
        get() = R.layout.view_item_camera
}