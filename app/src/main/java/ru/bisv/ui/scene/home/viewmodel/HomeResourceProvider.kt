package ru.bisv.ui.scene.home.viewmodel

interface HomeResourceProvider {

    fun provideDefaultErrorText(): CharSequence

}