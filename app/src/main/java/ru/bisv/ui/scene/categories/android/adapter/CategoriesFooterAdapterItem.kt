package ru.bisv.ui.scene.categories.android.adapter

import ru.bisv.R
import ru.frosteye.ovsa.adapter.StaticAdapterItemImpl

class CategoriesFooterAdapterItem : StaticAdapterItemImpl<CategoriesFooterView>() {

    override val layoutResourceId: Int
        get() = R.layout.view_item_categories_footer
}