package ru.bisv.ui.scene.player.android.dto

import android.widget.ImageView
import ru.bisv.api.model.ScCamera

data class CameraWithPreviewView(
    val camera: ScCamera,
    val view: ImageView
)
