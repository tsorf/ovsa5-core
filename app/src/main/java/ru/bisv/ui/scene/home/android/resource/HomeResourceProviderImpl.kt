package ru.bisv.ui.scene.home.android.resource

import android.content.Context
import ru.bisv.R
import ru.bisv.ui.scene.home.viewmodel.HomeResourceProvider
import javax.inject.Inject

class HomeResourceProviderImpl @Inject constructor(
    private val context: Context
) : HomeResourceProvider {

    override fun provideDefaultErrorText(): CharSequence {
        return context.getString(R.string.default_error)
    }
}