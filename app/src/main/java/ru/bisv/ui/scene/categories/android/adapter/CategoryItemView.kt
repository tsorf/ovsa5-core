package ru.bisv.ui.scene.categories.android.adapter

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import ru.bisv.databinding.ViewItemCategoryBinding
import ru.bisv.ui.common.images.DefaultImageLoader
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.frosteye.ovsa.adapter.AdapterEvent
import ru.frosteye.ovsa.adapter.AdapterModelView
import ru.frosteye.ovsa.adapter.ModelViewHolder
import ru.frosteye.ovsa.commons.delegate.lateinit.later

class CategoryItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), AdapterModelView<CategoriesContract.CategoryItem> {

    private lateinit var root: ViewItemCategoryBinding
    override var viewHolder: ModelViewHolder? = null

    override var model: CategoriesContract.CategoryItem by later {
        root.title.text = it.title.toString().capitalize()
        DefaultImageLoader.loadImage(it.image).into(root.icon)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        root = ViewItemCategoryBinding.bind(this)

        setOnClickListener {
            viewHolder?.publishEvent(AdapterEvent.Select(model))
        }
    }
}