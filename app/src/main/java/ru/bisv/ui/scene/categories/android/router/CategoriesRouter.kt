package ru.bisv.ui.scene.categories.android.router

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.bisv.api.model.ScCameraMetadata
import ru.bisv.api.model.ScCameraScope
import ru.bisv.ui.scene.cameras.android.view.CamerasFragment
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.frosteye.ovsa.commons.extensions.put
import ru.frosteye.ovsa.commons.router.DefaultActivityRouter

class CategoriesRouter(
    activity: AppCompatActivity
) : DefaultActivityRouter(activity), CategoriesContract.Router {

    override fun navigateToCameras(category: CategoriesContract.CategoryItem) {
        requireFragmentStack().apply {
            addFragment(CamerasFragment().also {
                val (scope, title) = when(category) {
                    is CategoriesContract.CategoryItem.Favorites -> {
                        ScCameraScope.favorites() to category.title
                    }
                    is CategoriesContract.CategoryItem.All -> {
                        ScCameraScope.all() to category.title
                    }
                    is CategoriesContract.CategoryItem.Tag -> {
                        ScCameraScope.tag(category.tag.uuid) to category.title
                    }
                }
                val meta = ScCameraMetadata(title)
                it.arguments = Bundle().apply {
                    put(scope)
                    put(meta)
                }
            })
        }
    }
}