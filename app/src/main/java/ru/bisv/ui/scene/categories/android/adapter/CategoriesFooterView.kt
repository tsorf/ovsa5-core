package ru.bisv.ui.scene.categories.android.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.AttributeSet
import android.widget.LinearLayout
import ru.bisv.R
import ru.bisv.databinding.ViewItemCategoriesFooterBinding
import ru.frosteye.ovsa.adapter.AdapterView
import ru.frosteye.ovsa.adapter.ModelViewHolder
import ru.frosteye.ovsa.commons.extensions.safeCast
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.binding

class CategoriesFooterView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr),
    AdapterView,
    BindingView<ViewItemCategoriesFooterBinding> {

    override var viewHolder: ModelViewHolder? = null

    override val boundView by binding { this }

    override fun onFinishInflate() {
        super.onFinishInflate()

        boundView.officialSite.setOnClickListener {
            openUrl(resources.getString(R.string.link_bs))
        }
    }

    private fun openUrl(url: String) {
        context.safeCast<Activity>()?.let {
            it.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
    }
}