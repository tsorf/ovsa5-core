package ru.bisv.ui.scene.home

import androidx.lifecycle.LiveData
import ru.frosteye.ovsa.mvvm.router.MvvmRouter
import ru.frosteye.ovsa.mvvm.view.BasicView
import ru.frosteye.ovsa.mvvm.viewmodel.LiveViewModel

object HomeContract {

    sealed class ViewState {

        object Loading : ViewState()

        object Ready : ViewState()

        data class LoadingError(
            val message: CharSequence
        ) : ViewState()
    }

    interface View : BasicView<Router>

    interface ViewModel : LiveViewModel {

        val state: LiveData<ViewState>

        fun initialize()
    }

    interface Router : MvvmRouter {

        fun navigateToCategories()
    }
}