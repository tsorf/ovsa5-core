package ru.bisv.ui.scene.player.android.router

import androidx.appcompat.app.AppCompatActivity
import ru.bisv.ui.scene.player.PlayerContract
import ru.frosteye.ovsa.commons.router.DefaultActivityRouter

class PlayerRouter(
    activity: AppCompatActivity
) : DefaultActivityRouter(activity), PlayerContract.Router {


}