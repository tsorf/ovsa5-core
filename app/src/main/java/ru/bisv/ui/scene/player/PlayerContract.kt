package ru.bisv.ui.scene.player

import android.os.Parcelable
import androidx.lifecycle.LiveData
import kotlinx.parcelize.Parcelize
import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraScope
import ru.bisv.ui.scene.player.viewmodel.PlayerNodeManager
import ru.frosteye.ovsa.mvvm.router.MvvmRouter
import ru.frosteye.ovsa.mvvm.view.BasicView
import ru.frosteye.ovsa.mvvm.viewmodel.LiveViewModel

object PlayerContract {

    @Parcelize
    data class Argument(
        val selectedCamera: ScCamera,
        val scope: ScCameraScope
    ) : Parcelable

    sealed class ViewState {

        data class FirstCamera(
            val camera: ScCamera
        ) : ViewState()

        data class WithNodes(
            val manager: PlayerNodeManager
        ) : ViewState()

        data class Error(
            val message: CharSequence
        ) : ViewState()
    }

    data class Node(
        val previous: ScCamera?,
        val next: ScCamera?,
    )

    sealed class Direction {

        object Next : Direction()

        object Previous : Direction()
    }

    interface View : BasicView<Router>

    interface ViewModel : LiveViewModel {

        val state: LiveData<ViewState>

        fun initialize(argument: Argument)
    }

    interface Router : MvvmRouter {

    }
}