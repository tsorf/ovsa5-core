package ru.bisv.ui.scene.categories.android.adapter

import ru.bisv.R
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.frosteye.ovsa.adapter.AdapterItemImpl

class CategoryAdapterItem(model: CategoriesContract.CategoryItem) :
    AdapterItemImpl<CategoriesContract.CategoryItem, CategoryItemView>(model) {

    override val layoutResourceId: Int
        get() = R.layout.view_item_category
}