package ru.bisv.ui.scene.cameras

import android.os.Parcelable
import androidx.lifecycle.LiveData
import kotlinx.parcelize.Parcelize
import ru.bisv.api.model.ScBanner
import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraScope
import ru.bisv.api.model.ScTag
import ru.frosteye.ovsa.mvvm.router.MvvmRouter
import ru.frosteye.ovsa.mvvm.view.BasicView
import ru.frosteye.ovsa.mvvm.viewmodel.LiveViewModel

object CamerasContract {

    sealed class ViewState {

        data class Ready(
            val cameras: List<CameraGridItem>
        ) : ViewState()
    }

    sealed class CameraGridItem {

        data class Camera(
            val camera: ScCamera
        ) : CameraGridItem()

        data class Banner(
            val banner: ScBanner
        ) : CameraGridItem()
    }

    interface CameraInfo {

        val camera: ScCamera

        val scope: ScCameraScope
    }

    interface View : BasicView<Router>

    interface ViewModel : LiveViewModel {

        val state: LiveData<ViewState>

        fun requestCameras(scope: ScCameraScope)
    }

    interface Router : MvvmRouter {

        fun navigateToCamera(cameraInfo: CameraInfo)
    }
}