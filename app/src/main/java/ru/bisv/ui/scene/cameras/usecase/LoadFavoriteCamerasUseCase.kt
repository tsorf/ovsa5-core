package ru.bisv.ui.scene.cameras.usecase

import io.reactivex.rxjava3.core.Observable
import ru.bisv.api.model.ScCamera
import ru.bisv.db.domain.CamerasRepo
import ru.frosteye.ovsa.commons.domain.usecase.UseCase
import ru.frosteye.ovsa.commons.rx.schedule.async
import javax.inject.Inject

class LoadFavoriteCamerasUseCase @Inject constructor(
    private val camerasRepo: CamerasRepo
) : UseCase<Void?, List<ScCamera>> {

    override fun build(param: Void?): Observable<List<ScCamera>> {
        return Observable.fromCallable {
            camerasRepo.findFavorites()
        }.async()
    }
}