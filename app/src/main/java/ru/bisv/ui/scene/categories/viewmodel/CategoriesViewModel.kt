package ru.bisv.ui.scene.categories.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.bisv.ui.scene.categories.usecase.LoadCamerasAndTagsUseCase
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.frosteye.ovsa.commons.rx.schedule.observeOnUi
import ru.frosteye.ovsa.mvvm.viewmodel.AbstractViewModel
import javax.inject.Inject

class CategoriesViewModel @Inject constructor(
    private val loadCamerasAndTagsUseCase: LoadCamerasAndTagsUseCase,
    private val categoriesResourceProvider: CategoriesResourceProvider
) : AbstractViewModel(), CategoriesContract.ViewModel {

    private val _categories: MutableLiveData<List<CategoriesContract.CategoryItem>> =
        MutableLiveData()
    override val categories: LiveData<List<CategoriesContract.CategoryItem>> = _categories

    private var isInitialContentLoaded = false

    override fun refreshData() {
        postControlsState(false)
        +loadCamerasAndTagsUseCase.build(null)
            .map {
                prepareCategories(it.tags)
            }
            .observeOnUi()
            .subscribe({
                postControlsState(true)
                isInitialContentLoaded = true
                _categories.value = it
            }, {
                postControlsState(true)
                postError(it.message)
            })
    }

    private fun prepareCategories(tags: List<CategoriesContract.ScTagWithPreview>): List<CategoriesContract.CategoryItem> {
        return mutableListOf<CategoriesContract.CategoryItem>().apply {
            val favoritesDesc = categoriesResourceProvider.provideFavoritesDescription()
            add(
                CategoriesContract.CategoryItem.Favorites(
                    favoritesDesc.first,
                    favoritesDesc.second
                )
            )
            val addDesc = categoriesResourceProvider.provideAllCamerasDescription()
            add(CategoriesContract.CategoryItem.All(addDesc.first, addDesc.second))
            addAll(tags
                .asSequence()
                .filter {
                    it.tag.hash != "action"
                }
                .map {
                    val tag = it.tag
                    CategoriesContract.CategoryItem.Tag(tag.name, it.preview, tag)
                }
                .toList()
            )
        }
    }
}