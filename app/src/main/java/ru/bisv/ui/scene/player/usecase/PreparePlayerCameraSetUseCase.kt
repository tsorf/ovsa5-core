package ru.bisv.ui.scene.player.usecase

import io.reactivex.rxjava3.core.Observable
import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraScope
import ru.bisv.db.domain.CamerasRepo
import ru.bisv.ui.scene.player.PlayerContract
import ru.bisv.ui.scene.player.viewmodel.PlayerNodeManager
import ru.bisv.ui.scene.player.viewmodel.PlayerResourceProvider
import ru.frosteye.ovsa.commons.domain.usecase.UseCase
import ru.frosteye.ovsa.commons.rx.schedule.async
import javax.inject.Inject

class PreparePlayerCameraSetUseCase @Inject constructor(
    private val camerasRepo: CamerasRepo,
    private val resourceProvider: PlayerResourceProvider
) : UseCase<PreparePlayerCameraSetUseCase.Param, PlayerContract.ViewState> {

    override fun build(param: Param): Observable<PlayerContract.ViewState> {
        return Observable.fromCallable {
            val currentCamera: ScCamera?
            val cameras: List<ScCamera>
            camerasRepo.db().transaction {
                val currentCamera = camerasRepo.findByUuid(param.currentCameraUuid)
                    ?: return@transaction PlayerContract.ViewState.Error(
                        resourceProvider.provideCameraNotFoundErrorMessage()
                    )
                val cameras = when (param.scope.constraint) {
                    ScCameraScope.Constraint.NONE -> {
                        camerasRepo.findAll()
                    }
                    ScCameraScope.Constraint.FAVORITES -> {
                        camerasRepo.findFavorites()
                    }
                    ScCameraScope.Constraint.TAG -> {
                        val tagUuid = param.scope.requiredTagUuid
                        camerasRepo.findByTag(tagUuid)
                    }
                }

                val index = cameras.indexOfFirst {
                    it.uuid == currentCamera.camera.uuid
                }
                if (index == -1) {
                    return@transaction PlayerContract.ViewState.Error(
                        resourceProvider.wrongCameraScope()
                    )
                }
                val manager = PlayerNodeManager(index, cameras)
                return@transaction PlayerContract.ViewState.WithNodes(manager)
            }

        }.async()
    }

    data class Param(
        val currentCameraUuid: String,
        val scope: ScCameraScope
    )
}