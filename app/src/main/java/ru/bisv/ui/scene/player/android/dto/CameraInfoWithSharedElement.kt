package ru.bisv.ui.scene.player.android.dto

import android.view.View
import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraScope
import ru.bisv.ui.scene.cameras.CamerasContract

class CameraInfoWithSharedElement(
    override val camera: ScCamera,
    override val scope: ScCameraScope,
    val sharedView: View
): CamerasContract.CameraInfo