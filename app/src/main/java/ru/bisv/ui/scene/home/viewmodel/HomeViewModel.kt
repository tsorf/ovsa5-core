package ru.bisv.ui.scene.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.bisv.ui.scene.home.HomeContract
import ru.bisv.ui.scene.home.usecase.InitializeUseCase
import ru.frosteye.ovsa.mvvm.viewmodel.AbstractViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val initializeUseCase: InitializeUseCase,
    private val resourceProvider: HomeResourceProvider
) : AbstractViewModel(), HomeContract.ViewModel {

    private val _state: MutableLiveData<HomeContract.ViewState> = MutableLiveData()
    override val state: LiveData<HomeContract.ViewState> = _state

    override fun initialize() {
        _state.value = HomeContract.ViewState.Loading
        +initializeUseCase.build(null)
            .subscribe({
                postControlsState(true)
                _state.value = HomeContract.ViewState.Ready
            }, {
                _state.value = HomeContract.ViewState.LoadingError(
                    it.message ?: resourceProvider.provideDefaultErrorText()
                )
            })

    }
}