package ru.bisv.ui.scene.categories.android.di

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.intrtl.lib2.di.domain.presenter.scope.UiScope
import dagger.Module
import dagger.Provides
import ru.bisv.ui.scene.categories.CategoriesContract
import ru.bisv.ui.scene.categories.android.router.CategoriesRouter
import ru.bisv.ui.scene.categories.android.view.CategoriesFragment
import ru.bisv.ui.scene.categories.viewmodel.CategoriesViewModel
import ru.frosteye.ovsa.commons.extensions.requireAppCompatActivity

@Module
internal class CategoriesModule {

    @Provides
    @UiScope
    fun viewModel(
        fragment: CategoriesFragment,
        factory: ViewModelProvider.Factory
    ): CategoriesContract.ViewModel {
        return fragment.viewModels<CategoriesViewModel> { factory }.value
    }

    @Provides
    @UiScope
    fun router(fragment: CategoriesFragment): CategoriesContract.Router {
        return CategoriesRouter(fragment.requireAppCompatActivity())
    }
}