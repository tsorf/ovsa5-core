package ru.bisv.ui.scene.home.android.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import ru.bisv.R
import ru.bisv.databinding.ActivityHomeBinding
import ru.bisv.ui.scene.categories.android.view.CategoriesFragment
import ru.bisv.ui.scene.home.HomeContract
import ru.frosteye.ovsa.commons.dagger.inject.Injectable
import ru.frosteye.ovsa.commons.extensions.observe
import ru.frosteye.ovsa.commons.messages.Messenger
import ru.frosteye.ovsa.commons.messages.defaultMessenger
import ru.frosteye.ovsa.commons.router.stack.DefaultFragmentStack
import ru.frosteye.ovsa.commons.router.stack.EmbeddingContainer
import ru.frosteye.ovsa.commons.router.stack.FragmentStack
import ru.frosteye.ovsa.commons.router.stack.FragmentStackHost
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.activityBinding
import javax.inject.Inject

class HomeActivity : AppCompatActivity(),
    HomeContract.View,
    BindingView<ActivityHomeBinding>,
    FragmentStackHost,
    Injectable {

    @Inject
    override lateinit var viewModel: HomeContract.ViewModel

    @Inject
    override lateinit var router: HomeContract.Router

    override lateinit var fragmentStack: FragmentStack

    override val boundView by activityBinding { layoutInflater }

    override val messenger by defaultMessenger()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(boundView.root)

        fragmentStack = DefaultFragmentStack(
            EmbeddingContainer(supportFragmentManager, boundView.container),
            listOf(CategoriesFragment())
        )

        observeMessenger(this)
        observeControlsState(this)
        observeViewState()

        viewModel.initialize()
    }

    private fun observeViewState() {
        observe(viewModel.state) {
            processState(it)
        }
    }

    private fun processState(it: HomeContract.ViewState) {
        when (it) {
            HomeContract.ViewState.Loading -> {
                boundView.loading.apply {
                    isVisible = true
                    isLoading = true
                }
            }
            HomeContract.ViewState.Ready -> {
                boundView.loading.apply {
                    isVisible = false
                    isLoading = false
                }
                router.navigateToCategories()
            }
            is HomeContract.ViewState.LoadingError -> {
                boundView.loading.apply {
                    isVisible = true
                    isLoading = false
                    setError(it.message)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (!fragmentStack.goBack()) {
            super.onBackPressed()
        }
    }
}