package ru.bisv.ui.scene.cameras.android.di

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.intrtl.lib2.di.domain.presenter.scope.UiScope
import dagger.Module
import dagger.Provides
import ru.bisv.ui.scene.cameras.CamerasContract
import ru.bisv.ui.scene.cameras.android.router.CamerasRouter
import ru.bisv.ui.scene.cameras.android.view.CamerasFragment
import ru.bisv.ui.scene.cameras.viewmodel.CamerasViewModel
import ru.frosteye.ovsa.commons.extensions.requireAppCompatActivity

@Module
internal class CamerasModule {

    @Provides
    @UiScope
    fun viewModel(
        fragment: CamerasFragment,
        factory: ViewModelProvider.Factory
    ): CamerasContract.ViewModel {
        return fragment.viewModels<CamerasViewModel> { factory }.value
    }

    @Provides
    @UiScope
    fun router(fragment: CamerasFragment): CamerasContract.Router {
        return CamerasRouter(fragment.requireAppCompatActivity())
    }
}