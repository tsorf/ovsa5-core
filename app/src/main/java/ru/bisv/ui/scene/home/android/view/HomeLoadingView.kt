package ru.bisv.ui.scene.home.android.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.view.isVisible
import ru.bisv.R
import ru.bisv.databinding.ViewStartLoadingBinding
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.binding

class HomeLoadingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr),
    BindingView<ViewStartLoadingBinding> {

    override val boundView by binding { this }

    var isLoading: Boolean = false
        set(value) {
            field = value
            boundView.loadingErrorMessage.isVisible = value
            if (isLoading) {
                boundView.apply {
                    loadingError.isVisible = false
                    retry.isVisible = false
                    progress.isVisible = true
                    preparingLabel.isVisible = true
                }
            } else {
                boundView.apply {
                    progress.isVisible = false
                    preparingLabel.isVisible = false
                }
            }
        }

    init {
        inflate(context, R.layout.view_start_loading, this)
    }

    fun clearError() {
        boundView.loadingError.isVisible = false
        boundView.loadingErrorMessage.text = null
    }

    fun setError(message: CharSequence) {
        boundView.loadingError.isVisible = true
        boundView.loadingErrorMessage.text = message
    }
}