package ru.bisv.ui.scene.categories

import androidx.lifecycle.LiveData
import ru.bisv.api.model.ScBanner
import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScTag
import ru.frosteye.ovsa.commons.router.viewstack.StackEntry
import ru.frosteye.ovsa.mvvm.router.MvvmRouter
import ru.frosteye.ovsa.mvvm.view.BasicView
import ru.frosteye.ovsa.mvvm.viewmodel.LiveViewModel
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

object CategoriesContract {

    sealed class CategoryItem(
        val title: CharSequence,
        val image: ImagePreview?
    ) {

        class Favorites(
            title: CharSequence,
            image: ImagePreview?
        ) : CategoryItem(title, image)

        class All(
            title: CharSequence,
            image: ImagePreview?
        ) : CategoryItem(title, image)

        class Tag(
            title: CharSequence,
            image: ImagePreview?,
            val tag: ScTag
        ) : CategoryItem(title, image)
    }

    data class ScTagWithPreview(
        val tag: ScTag,
        val preview: ImagePreview?
    )

    interface View : BasicView<Router>

    interface ViewModel : LiveViewModel {

        val categories: LiveData<List<CategoryItem>>

        fun refreshData()
    }

    interface Router : MvvmRouter {

        fun navigateToCameras(category: CategoryItem)
    }
}