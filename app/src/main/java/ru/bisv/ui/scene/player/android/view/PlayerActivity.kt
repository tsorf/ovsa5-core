package ru.bisv.ui.scene.player.android.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import ru.bisv.R
import ru.bisv.api.model.ScCamera
import ru.bisv.databinding.ActivityPlayerBinding
import ru.bisv.ui.scene.player.PlayerContract
import ru.frosteye.ovsa.commons.dagger.inject.Injectable
import ru.frosteye.ovsa.commons.extensions.observe
import ru.frosteye.ovsa.commons.extensions.requiredArgument
import ru.frosteye.ovsa.commons.messages.defaultMessenger
import ru.frosteye.ovsa.commons.rx.disposable.DisposableTrashCan
import ru.frosteye.ovsa.commons.rx.disposable.SimpleDisposableTrashCan
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.activityBinding
import javax.inject.Inject

class PlayerActivity : AppCompatActivity(),
    PlayerContract.View,
    DisposableTrashCan by SimpleDisposableTrashCan(),
    BindingView<ActivityPlayerBinding>,
    Injectable {

    @Inject
    override lateinit var viewModel: PlayerContract.ViewModel

    @Inject
    override lateinit var router: PlayerContract.Router

    private val argument by requiredArgument<PlayerContract.Argument>()

    override val boundView by activityBinding { layoutInflater }

    override val messenger by defaultMessenger()

    private lateinit var state: PlayerContract.ViewState
    private lateinit var playbackState: PlaybackState

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(boundView.root)
        supportPostponeEnterTransition()

        ViewCompat.setTransitionName(boundView.preview, argument.selectedCamera.uuid)

        observeMessenger(this)
        observeControlsState(this)
        observeViewState()

        processState(PlayerContract.ViewState.FirstCamera(argument.selectedCamera))

        viewModel.initialize(argument)
    }

    private fun observeViewState() {
        observe(viewModel.state) {
            processState(it)
        }
    }

    private fun processState(state: PlayerContract.ViewState) {
        this.state = state
        when (state) {
            is PlayerContract.ViewState.Error -> {

            }
            is PlayerContract.ViewState.FirstCamera -> {
                playbackState = PlaybackState(state.camera, null)
                play(playbackState.camera)
            }
            is PlayerContract.ViewState.WithNodes -> {

            }
        }
    }

    private fun play(camera: ScCamera) {

    }

    private data class PlaybackState(
        val camera: ScCamera,
        val node: PlayerContract.Node?
    )
}