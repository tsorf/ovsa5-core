package ru.bisv.ui.scene.player.viewmodel

interface PlayerResourceProvider {

    fun provideCameraNotFoundErrorMessage(): CharSequence

    fun wrongCameraScope(): CharSequence
}