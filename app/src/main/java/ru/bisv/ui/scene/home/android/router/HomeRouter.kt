package ru.bisv.ui.scene.home.android.router

import androidx.appcompat.app.AppCompatActivity
import ru.bisv.ui.scene.home.HomeContract
import ru.frosteye.ovsa.commons.router.DefaultActivityRouter

class HomeRouter(
    activity: AppCompatActivity
) : DefaultActivityRouter(activity), HomeContract.Router {

    override fun navigateToCategories() {

    }

}