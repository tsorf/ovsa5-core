package ru.bisv.ui.scene.player.viewmodel

import ru.bisv.api.model.ScCamera
import ru.bisv.ui.scene.player.PlayerContract

class PlayerNodeManager(
    selectedCameraIndex: Int,
    private val dataSet: List<ScCamera>
) {

    private val dataSetSize = dataSet.size
    private var cursor = -1
    var currentNode: PlayerContract.Node
        private set

    init {
        check(selectedCameraIndex > -1 && selectedCameraIndex < dataSetSize) {
            "selected camera index $selectedCameraIndex is not found in dataSet"
        }
        cursor = selectedCameraIndex
        currentNode = createNodeInner()
    }

    private fun createNodeInner(): PlayerContract.Node {
        val previousIndex = cursor - 1
        val nextIndex = cursor + 1
        val previous: ScCamera? = if (previousIndex < 0) null else dataSet[previousIndex]
        val next: ScCamera? = if (nextIndex == dataSetSize) null else dataSet[nextIndex]
        return PlayerContract.Node(previous, next)
    }

    fun canGoTo(direction: PlayerContract.Direction): Boolean {
        return when (direction) {
            PlayerContract.Direction.Next -> currentNode.next != null
            PlayerContract.Direction.Previous -> currentNode.previous != null
        }
    }

    fun next() {
        if (canGoTo(PlayerContract.Direction.Next)) {
            cursor++
            createNodeInner()
        }

    }

    fun previous() {
        if (canGoTo(PlayerContract.Direction.Previous)) {
            cursor--
            createNodeInner()
        }
    }
}