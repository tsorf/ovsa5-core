package ru.bisv.ui.scene.home.usecase

import io.reactivex.rxjava3.core.Observable
import ru.bisv.db.domain.CamerasRepo
import ru.bisv.domain.camera.data.mapper.ApiRemoteDataMapper
import ru.bisv.networking.api.CamerasApi
import ru.frosteye.ovsa.commons.domain.usecase.UseCase
import ru.frosteye.ovsa.commons.rx.schedule.observeOnUi
import ru.frosteye.ovsa.commons.rx.schedule.subscribeOnIo
import javax.inject.Inject

class InitializeUseCase @Inject constructor(
    private val camerasRepo: CamerasRepo,
    private val camerasApi: CamerasApi,
    private val mapper: ApiRemoteDataMapper
) : UseCase<Void?, InitializeUseCase.Result> {


    override fun build(param: Void?): Observable<Result> {

        return Observable.fromCallable {
            camerasRepo.checkIfLoaded()
        }.subscribeOnIo()
            .flatMap {
                if (it) {
                    Observable.just(Result.Success)
                } else {
                    camerasApi.getCameras()
                        .subscribeOnIo()
                        .toObservable()
                        .map { response ->
                            val data = mapper.convert(response.cameras, response.tags)
                            camerasRepo.updateCamerasAndTags(data)
                            Result.Success
                        }
                }
            }
            .cast(Result::class.java)
            .observeOnUi()
    }

    sealed class Result {

        object Success : Result()
    }
}