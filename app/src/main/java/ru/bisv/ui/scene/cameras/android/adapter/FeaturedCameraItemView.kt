package ru.bisv.ui.scene.cameras.android.adapter

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import ru.bisv.databinding.ViewItemCameraFeaturedBinding
import ru.bisv.ui.common.images.DefaultImageLoader
import ru.bisv.ui.scene.cameras.CamerasContract
import ru.bisv.ui.scene.player.android.dto.CameraWithPreviewView
import ru.frosteye.ovsa.adapter.AdapterEvent
import ru.frosteye.ovsa.adapter.AdapterModelView
import ru.frosteye.ovsa.adapter.ModelViewHolder
import ru.frosteye.ovsa.commons.delegate.lateinit.later
import ru.frosteye.ovsa.commons.ui.binding.BindingView
import ru.frosteye.ovsa.commons.ui.binding.binding
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

class FeaturedCameraItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr),
    AdapterModelView<CamerasContract.CameraGridItem.Camera>,
    BindingView<ViewItemCameraFeaturedBinding> {

    override val boundView by binding { this }
    override var viewHolder: ModelViewHolder? = null

    override var model: CamerasContract.CameraGridItem.Camera by later {
        boundView.title.text = it.camera.name.capitalize()
        boundView.id.text = it.camera.streamId
        boundView.tags.text = it.camera.tags.joinToString(", ") { tag -> tag }.capitalize()
        val preview = ImagePreview.Remote(it.camera.thumbnail.url)
        DefaultImageLoader.loadImage(preview)
            .centerCrop()
            .into(boundView.preview)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        setOnClickListener {
            viewHolder?.publishEvent(AdapterEvent.Select(
                CameraWithPreviewView(model.camera, boundView.preview)
            ))
        }
    }
}