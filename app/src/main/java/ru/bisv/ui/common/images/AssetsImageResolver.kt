package ru.bisv.ui.common.images

import android.content.Context
import ru.frosteye.ovsa.commons.extensions.md5
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview
import java.io.File

class AssetsImageResolver constructor(
    private val context: Context,
    private val baseUrl: String
) : ImageResolver {

    override fun resolve(path: String): ImagePreview? {
        return try {
            val file = getFileFromAssets(path)
            ImagePreview.Local(file)
        } catch (e: Exception) {
            e.printStackTrace()
            ImagePreview.Remote("${baseUrl}${path}")
        }
    }

    private fun getFileFromAssets(path: String): File {
        val fileName = "${path.md5()}.png"
        return File(context.cacheDir, fileName)
            .also {
                if (!it.exists()) {
                    val assetsPath = path.let { originalPath ->
                        if (originalPath.startsWith("/")) {
                            originalPath.substring(1)
                        } else {
                            originalPath
                        }
                    }
                    it.outputStream().use { cache ->
                        context.assets.open(assetsPath).use { inputStream ->
                            inputStream.copyTo(cache)
                        }
                    }
                }
            }
    }
}