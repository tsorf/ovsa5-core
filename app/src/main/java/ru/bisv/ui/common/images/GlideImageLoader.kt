package ru.bisv.ui.common.images

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import ru.frosteye.ovsa.commons.exceptions.wrongWay
import ru.frosteye.ovsa5.commons.images.loader.ImageLoader
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

@SuppressLint("StaticFieldLeak")
class GlideImageLoader : ImageLoader {

    override fun loadImage(image: ImagePreview?): ImageLoader.Builder {
        return BuilderImpl(image)
    }

    class BuilderImpl(
        private val image: ImagePreview?
    ) : ImageLoader.Builder {

        private var centerCrop = false
        private var alternative: ImagePreview? = null

        override fun centerCrop(): ImageLoader.Builder {
            this.centerCrop = true
            return this
        }

        override fun alternative(alternativePreview: ImagePreview): ImageLoader.Builder {
            this.alternative = alternativePreview
            return this
        }

        override fun into(imageView: ImageView) {
            val context = imageView.context
            if (image == null) {
                imageView.setImageDrawable(null)
                return
            }
            when (image) {
                is ImagePreview.Local -> builder(context, image).into(imageView)
                is ImagePreview.Raw -> setBitmap(imageView, image.bitmap)
                is ImagePreview.Remote -> builder(context, image).into(imageView)
                is ImagePreview.Resource -> setDrawableRes(imageView, image.drawableRes)
                else -> wrongWay()
            }
        }

        fun builder(context: Context, preview: ImagePreview): RequestBuilder<Drawable> {
            var builder = when (preview) {
                is ImagePreview.Local -> {
                    Glide.with(context).load(preview.file)
                }
                is ImagePreview.Raw -> {
                    Glide.with(context).load(preview.bitmap)
                }
                is ImagePreview.Remote -> {
                    Glide.with(context).load(preview.url)
                }
                is ImagePreview.Resource -> {
                    Glide.with(context).load(preview.drawableRes)
                }
                else -> wrongWay()
            }
                .diskCacheStrategy(DiskCacheStrategy.ALL)
            if (centerCrop) {
                builder = builder.centerCrop()
            }
            val alternativePreview = alternative
            if (alternativePreview != null) {
                builder = builder.error(
                    BuilderImpl(alternativePreview).apply {
                        if (centerCrop) {
                            centerCrop()
                        }
                    }

                )
            }
            return builder
        }

        private fun setBitmap(imageView: ImageView, bitmap: Bitmap?) {
            if (bitmap == null) {
                imageView.setImageBitmap(null)
                return
            }
            if (imageView.drawable == null) {
                imageView.setImageBitmap(bitmap)
            } else {
                //TODO
                imageView.setImageBitmap(bitmap)
            }
        }

        private fun setDrawableRes(imageView: ImageView, drawableRes: Int?) {
            if (drawableRes == null) {
                imageView.setImageBitmap(null)
                return
            }
            if (imageView.drawable == null) {
                imageView.setImageResource(drawableRes)
            } else {
                //TODO
                imageView.setImageResource(drawableRes)
            }
        }
    }
}