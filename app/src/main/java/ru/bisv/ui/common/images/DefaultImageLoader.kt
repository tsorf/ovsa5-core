package ru.bisv.ui.common.images

import ru.frosteye.ovsa5.commons.images.loader.ImageLoader
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

object DefaultImageLoader : ImageLoader  {

    private var apiUrl: String? = null
    private val realLoader = GlideImageLoader()

    fun init(apiUrl: String) {
        this.apiUrl = apiUrl
    }

    override fun loadImage(image: ImagePreview?): ImageLoader.Builder {
        val baseUrl = apiUrl
        check(baseUrl != null)
        var preview = image
        if (preview != null && preview is ImagePreview.Remote) {
            if (preview.url.startsWith("/")) {
                preview = ImagePreview.Remote("${baseUrl}${preview.url}")
            }
        }
        return realLoader.loadImage(preview)
    }
}