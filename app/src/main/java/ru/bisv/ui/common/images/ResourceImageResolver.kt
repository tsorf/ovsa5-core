package ru.bisv.ui.common.images

import android.content.Context
import ru.bisv.R
import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

class ResourceImageResolver(
    private val context: Context,
    private val baseUrl: String
) : ImageResolver {

    private val resourceMap = mapOf(
        "/img/tags/favorites.png" to R.drawable.tag_favorites,
        "/img/tags/Vse.png" to R.drawable.tag_all,
        "/img/tags/gorod.png" to R.drawable.tag_city,
        "/img/tags/Dorogi.png" to R.drawable.tag_roads,
        "/img/tags/more.png" to R.drawable.tag_sea,
        "/img/tags/abxazia.png" to R.drawable.tag_abkhasia,
        "/img/tags/camera LTZ.png" to R.drawable.tag_ltz,
        "/img/tags/camera zv.png" to R.drawable.tag_sound,
        "/img/tags/goru.png" to R.drawable.tag_mountains,
        "/img/tags/Krasnaya Polyana.png" to R.drawable.tag_mountains,
        "/img/tags/port.png" to R.drawable.tag_port,
        "/img/tags/ruba.png" to R.drawable.tag_fish,
        "/img/tags/zgivotnue.png" to R.drawable.tag_animals,
        "/img/tags/pushok.png" to R.drawable.tag_pushok,
        "/img/tags/tv.png" to R.drawable.tag_tv,
    )

    override fun resolve(path: String): ImagePreview {
        val resource = resourceMap[path]
        return if (resource != null) {
            ImagePreview.Resource(resource)
        } else {
            ImagePreview.Remote("${baseUrl}${path}")
        }
    }
}