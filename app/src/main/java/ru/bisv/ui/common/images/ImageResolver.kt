package ru.bisv.ui.common.images

import ru.frosteye.ovsa5.commons.images.preview.ImagePreview

interface ImageResolver {

    fun resolve(path: String): ImagePreview?
}