package com.intrtl.lib2.di.domain.presenter.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class UiScope {
}