package ru.bisv.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import ru.bisv.app.ScApp
import ru.bisv.db.room.di.module.RoomDbModule
import ru.bisv.di.module.AppModule
import ru.bisv.di.module.MapperModule
import ru.bisv.di.module.UiModule
import ru.bisv.networking.di.module.RestModule
import ru.bisv.networking.di.qualifier.ScRestApiUrl
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        MapperModule::class,
        UiModule::class,
        RoomDbModule::class,
        RestModule::class
    ]
)
interface AppComponent {

    @ScRestApiUrl
    fun restApiUrl(): String

    fun inject(application: ScApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: ScApp): Builder

        fun roomDbModule(module: RoomDbModule): Builder

        fun build(): AppComponent
    }
}