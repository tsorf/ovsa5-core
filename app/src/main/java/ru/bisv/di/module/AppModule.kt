package ru.bisv.di.module

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import dagger.android.DispatchingAndroidInjector
import ru.bisv.app.ScApp
import ru.bisv.networking.di.qualifier.ScRestApiUrl
import ru.bisv.ui.common.images.AssetsImageResolver
import ru.bisv.ui.common.images.ImageResolver
import ru.bisv.ui.common.images.ResourceImageResolver
import ru.frosteye.ovsa.mvvm.di.CommonViewModelFactory
import ru.frosteye.ovsa.mvvm.di.ComponentHandler
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {

    @Provides
    @Singleton
    fun provideApplication(application: ScApp): Application = application

    @Provides
    @Singleton
    fun provideContext(application: ScApp): Context = application

    @Provides
    @Singleton
    fun imageResolver(
        @ScRestApiUrl apiUrl: String,
        context: Context
    ): ImageResolver {
        return ResourceImageResolver(context, apiUrl)
    }

    @Provides
    @Singleton
    fun provideComponentHandler(
        application: Application,
        injector: DispatchingAndroidInjector<Any>
    ): ComponentHandler {
        return ComponentHandler(application, injector)
    }

    @Provides
    @Singleton
    fun bindViewModelFactory(factory: CommonViewModelFactory): ViewModelProvider.Factory = factory
}