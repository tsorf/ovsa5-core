package ru.bisv.di.module

import dagger.Binds
import dagger.Module
import ru.bisv.ui.scene.cameras.android.resource.CamerasResourceProviderImpl
import ru.bisv.ui.scene.cameras.viewmodel.CamerasResourceProvider
import ru.bisv.ui.scene.categories.android.resource.CategoriesResourceProviderImpl
import ru.bisv.ui.scene.categories.viewmodel.CategoriesResourceProvider
import ru.bisv.ui.scene.home.android.resource.HomeResourceProviderImpl
import ru.bisv.ui.scene.home.viewmodel.HomeResourceProvider
import ru.bisv.ui.scene.player.android.resource.PlayerResourceProviderImpl
import ru.bisv.ui.scene.player.viewmodel.PlayerResourceProvider
import javax.inject.Singleton

@Module
abstract class ResourcesModule {

    @Binds
    @Singleton
    abstract fun categories(impl: CategoriesResourceProviderImpl): CategoriesResourceProvider

    @Binds
    @Singleton
    abstract fun cameras(impl: CamerasResourceProviderImpl): CamerasResourceProvider

    @Binds
    @Singleton
    abstract fun home(impl: HomeResourceProviderImpl): HomeResourceProvider

    @Binds
    @Singleton
    abstract fun player(impl: PlayerResourceProviderImpl): PlayerResourceProvider
}