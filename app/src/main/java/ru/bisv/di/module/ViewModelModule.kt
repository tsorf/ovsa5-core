package ru.bisv.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.bisv.ui.scene.cameras.viewmodel.CamerasViewModel
import ru.bisv.ui.scene.categories.viewmodel.CategoriesViewModel
import ru.bisv.ui.scene.home.viewmodel.HomeViewModel
import ru.bisv.ui.scene.player.viewmodel.PlayerViewModel
import ru.frosteye.ovsa.commons.dagger.support.ViewModelKey

@Suppress("unused")
@Module(
    includes = [
        ResourcesModule::class
    ]
)
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun home(vm: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoriesViewModel::class)
    abstract fun categories(vm: CategoriesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CamerasViewModel::class)
    abstract fun cameras(vm: CamerasViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PlayerViewModel::class)
    abstract fun player(vm: PlayerViewModel): ViewModel
}
