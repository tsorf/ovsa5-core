package ru.bisv.di.module

import dagger.Module
import dagger.Provides
import ru.bisv.domain.camera.data.mapper.ApiRemoteCameraWithTagIdsMapper
import ru.bisv.domain.camera.data.mapper.ApiRemoteDataMapper
import ru.bisv.domain.camera.data.mapper.DefaultApiRemoteDataMapper
import ru.bisv.domain.camera.data.mapper.ApiRemoteTagMapper
import ru.bisv.ui.common.images.ImageResolver

@Module
class MapperModule {

    @Provides
    fun provideRemoteDataMapper(imageResolver: ImageResolver): ApiRemoteDataMapper {
        return DefaultApiRemoteDataMapper(
            ApiRemoteCameraWithTagIdsMapper(),
            ApiRemoteTagMapper(imageResolver)
        )
    }
}