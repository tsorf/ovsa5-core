package ru.bisv.di.module

import com.intrtl.lib2.di.domain.presenter.scope.UiScope
import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.bisv.ui.scene.cameras.android.di.CamerasModule
import ru.bisv.ui.scene.cameras.android.view.CamerasFragment
import ru.bisv.ui.scene.categories.android.di.CategoriesModule
import ru.bisv.ui.scene.categories.android.view.CategoriesFragment
import ru.bisv.ui.scene.home.android.di.HomeModule
import ru.bisv.ui.scene.home.android.view.HomeActivity
import ru.bisv.ui.scene.player.android.di.PlayerModule
import ru.bisv.ui.scene.player.android.view.PlayerActivity

@Module
interface UiModule {

    @UiScope
    @ContributesAndroidInjector(modules = [HomeModule::class])
    fun home(): HomeActivity

    @UiScope
    @ContributesAndroidInjector(modules = [CategoriesModule::class])
    fun categories(): CategoriesFragment

    @UiScope
    @ContributesAndroidInjector(modules = [CamerasModule::class])
    fun cameras(): CamerasFragment

    @UiScope
    @ContributesAndroidInjector(modules = [PlayerModule::class])
    fun player(): PlayerActivity
}