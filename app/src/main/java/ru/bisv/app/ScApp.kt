package ru.bisv.app

import android.app.Application
import ru.frosteye.ovsa.mvvm.di.ComponentHandler
import ru.bisv.db.room.di.module.RoomDbModule
import ru.bisv.di.component.AppComponent
import ru.bisv.di.component.DaggerAppComponent
import ru.bisv.ui.common.images.DefaultImageLoader
import ru.frosteye.ovsa.commons.dagger.inject.Injectable
import javax.inject.Inject

class ScApp : Application() {

    @Inject
    lateinit var injector: ComponentHandler


    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder()
            .application(this)
            .roomDbModule(RoomDbModule(this))
            .build()
        component.inject(this)
        injector.registerForActivityType(Injectable::class.java)
        DefaultImageLoader.init(component.restApiUrl())
    }



    companion object {

        private lateinit var component: AppComponent
    }
}