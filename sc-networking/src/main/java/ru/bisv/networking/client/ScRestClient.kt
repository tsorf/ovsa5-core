package ru.bisv.networking.client

import ru.frosteye.ovsa.commons.networking.client.base.RetrofitCache
import ru.frosteye.ovsa.commons.networking.client.impl.RestClient
import ru.frosteye.ovsa.commons.networking.client.support.EndpointProvider
import ru.frosteye.ovsa.commons.networking.client.support.IdentityProvider

class ScRestClient(
    identityProvider: IdentityProvider,
    endpointProvider: EndpointProvider,
    retrofitCache: RetrofitCache,
    apis: Set<Class<*>>
) : RestClient(identityProvider, endpointProvider, retrofitCache, apis) {
}