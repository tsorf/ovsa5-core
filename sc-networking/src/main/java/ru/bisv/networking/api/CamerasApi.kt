package ru.bisv.networking.api

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import ru.bisv.networking.response.cameras.CamerasResponse
import ru.frosteye.ovsa.commons.networking.client.support.RestApi

@RestApi
interface CamerasApi {

    @GET("cams.json?platform=Android")
    fun getCameras(): Single<CamerasResponse>
}