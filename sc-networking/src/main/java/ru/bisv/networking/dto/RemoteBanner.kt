package ru.bisv.networking.dto

import com.google.gson.annotations.SerializedName

data class RemoteBanner(
    @SerializedName("url") val url: String,
    @SerializedName("href") val href: String,
    @SerializedName("timeout") val timeout: Int
)