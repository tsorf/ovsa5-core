package ru.bisv.networking.dto

import com.google.gson.annotations.SerializedName

data class RemoteCamera(
    @SerializedName("stream_id") val streamId: String,
    @SerializedName("text") val text: String,
    @SerializedName("name") val name: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("thumbnail") val thumbnail: String,
    @SerializedName("thumbnail_large") val thumbnailLarge: String,
    @SerializedName("search") val search: String?,
    @SerializedName("sd") val sdStreamUrl: String?,
    @SerializedName("hd") val hdStreamUrl: String?,
    @SerializedName("tags") val tags: List<String>,
    @SerializedName("alive") val isAlive: Boolean
)
