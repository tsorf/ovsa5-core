package ru.bisv.networking.dto

import com.google.gson.annotations.SerializedName

data class RemoteTag(
    @SerializedName("_id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("ico") val iconUrl: String,
    @SerializedName("hash") val hash: String,
    @SerializedName("sort") val sort: Int
)
