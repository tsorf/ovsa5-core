package ru.bisv.networking.response.cameras

import com.google.gson.annotations.SerializedName
import ru.bisv.networking.dto.RemoteBanner
import ru.bisv.networking.dto.RemoteCamera
import ru.bisv.networking.dto.RemoteTag

data class CamerasResponse(
    @SerializedName("token") val token: String,
    @SerializedName("about") val about: String,
    @SerializedName("banners") val banners: List<RemoteBanner>,
    @SerializedName("tags_array") val tags: List<RemoteTag>,
    @SerializedName("cams") val cameras: List<RemoteCamera>
)