package ru.bisv.networking.di.module

import dagger.Module
import dagger.Provides
import ru.bisv.networking.BuildConfig
import ru.bisv.networking.api.CamerasApi
import ru.bisv.networking.client.ScRestClient
import ru.bisv.networking.di.qualifier.ScRestApiUrl
import ru.frosteye.ovsa.commons.networking.client.base.ApiClient
import ru.frosteye.ovsa.commons.networking.client.base.RetrofitCache
import ru.frosteye.ovsa.commons.networking.client.support.EndpointProvider
import ru.frosteye.ovsa.commons.networking.client.support.IdentityProvider
import ru.frosteye.ovsa.commons.networking.client.support.SimpleTokenIdentityProvider
import ru.frosteye.ovsa.commons.networking.client.support.TokenIdentity
import javax.inject.Singleton

@Module
class RestModule {

    @Provides
    @Singleton
    @ScRestApiUrl
    fun provideRestApiUrl(): String {
        return BuildConfig.BASE_REST_URL
    }

    @Provides
    @Singleton
    fun provideIdentityProvider(): IdentityProvider {
        return SimpleTokenIdentityProvider(
            TokenIdentity(null)
        )
    }

    @Provides
    @Singleton
    fun provideEndpointProvider(
        @ScRestApiUrl baseUrl: String
    ): EndpointProvider {
        return object : EndpointProvider {
            override fun endpointForApi(apiClass: Class<*>): String {
                return baseUrl
            }
        }
    }

    @Provides
    @Singleton
    fun provideApiClient(
        endpointProvider: EndpointProvider,
        identityProvider: IdentityProvider,
    ): ApiClient {
        return ScRestClient(
            identityProvider,
            endpointProvider,
            RetrofitCache(),
            setOf(
                CamerasApi::class.java
            )
        )
    }

    @Provides
    @Singleton
    fun provideCamerasApi(client: ApiClient): CamerasApi = client.apiFor(CamerasApi::class.java)
}