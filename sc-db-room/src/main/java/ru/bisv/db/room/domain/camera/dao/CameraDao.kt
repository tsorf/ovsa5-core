package ru.bisv.db.room.domain.camera.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.bisv.db.room.dao.CudDao
import ru.bisv.db.room.domain.camera.model.RoomCamera
import ru.bisv.db.room.domain.camera.model.RoomCameraAndTag
import ru.bisv.db.room.domain.camera.model.RoomCameraFavorite
import ru.bisv.db.room.domain.camera.model.RoomCameraWithTags


@Dao
internal interface CameraDao : CudDao<RoomCamera> {

    @Query("SELECT COUNT(uuid) FROM camera")
    fun countCameras(): Int

    @Query("SELECT * FROM camera")
    fun findAll(): List<RoomCamera>

    @Query("SELECT * FROM camera WHERE stream_id = :streamId")
    fun findByStreamId(streamId: String): RoomCameraWithTags?

    @Query("SELECT * FROM camera WHERE uuid = :cameraUuid")
    fun findByUuid(cameraUuid: String): RoomCameraWithTags?

    @Query("SELECT stream_id FROM favorite")
    fun findFavoriteCamerasStreamIds(): List<String>

    @Query("DELETE FROM camera")
    fun deleteAll(): Int

    @Query("DELETE FROM camera WHERE uuid = :cameraUuid")
    fun deleteByUuid(cameraUuid: String)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun like(favorite: RoomCameraFavorite)

    @Query("DELETE FROM favorite WHERE stream_id = :streamId")
    fun unlike(streamId: String)

    @Insert
    fun connectCamerasAndTags(connections: List<RoomCameraAndTag>)
}