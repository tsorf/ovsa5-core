package ru.bisv.db.room.domain.camera.repo

import androidx.room.RoomDatabase
import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraWithTags
import ru.bisv.api.model.ScCameraWithTagsIds
import ru.bisv.api.model.ScTag
import ru.bisv.db.domain.CamerasRepo
import ru.bisv.db.domain.TagsRepo
import ru.bisv.db.room.domain.camera.dao.CameraDao
import ru.bisv.db.room.domain.camera.dao.TagDao
import ru.bisv.db.room.domain.camera.mapper.*
import ru.bisv.db.room.domain.camera.mapper.ApiRoomTagMapper
import ru.bisv.db.room.domain.camera.mapper.RoomTagMapper
import ru.bisv.db.room.domain.camera.model.RoomCameraAndTag
import ru.bisv.db.room.domain.camera.model.RoomTag
import ru.bisv.db.room.repo.RoomRepo

internal class RoomTagsRepo(
    database: RoomDatabase,
    private val tagDao: TagDao
) : RoomRepo(database), TagsRepo {

    private val roomTagMapper = RoomTagMapper()
    private val apiRoomTagMapper = ApiRoomTagMapper()

    override fun findAll(): List<ScTag> {
        return tagDao.findAll().map(apiRoomTagMapper::convert)
    }

}
