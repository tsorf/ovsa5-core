package ru.bisv.db.room.mapper

interface Mapper<From, To> {

    fun convert(source: From): To
}