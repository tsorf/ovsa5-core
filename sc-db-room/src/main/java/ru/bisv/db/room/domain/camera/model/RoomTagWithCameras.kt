package ru.bisv.db.room.domain.camera.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

internal data class RoomTagWithCameras(
    @Embedded
    val tag: RoomTag,
    @Relation(
        parentColumn = "uuid",
        entity = RoomCamera::class,
        entityColumn = "uuid",
        associateBy = Junction(
            value = RoomCameraAndTag::class,
            parentColumn = "tag_uuid",
            entityColumn = "camera_uuid"
        )
    )
    val cameras: List<RoomCamera>
)