package ru.bisv.db.room.domain.camera.mapper

import ru.bisv.api.model.ScCamera
import ru.bisv.api.model.ScCameraWithTags
import ru.bisv.api.model.ScTag
import ru.bisv.db.room.domain.camera.model.RoomCamera
import ru.bisv.db.room.domain.camera.model.RoomCameraWithTags
import ru.bisv.db.room.mapper.Mapper
import javax.inject.Inject

internal class ApiRoomCameraWithTagsMapper @Inject constructor(
    private val checkIfFavorite: (RoomCamera) -> Boolean
) : Mapper<RoomCameraWithTags, ScCameraWithTags> {

    override fun convert(source: RoomCameraWithTags): ScCameraWithTags {
        return ScCameraWithTags(
            ScCamera(
                source.camera.uuid,
                source.camera.streamId,
                source.camera.text,
                source.camera.name,
                source.camera.addedAt,
                ScCamera.Thumbnail(
                    source.camera.thumbnail.url,
                    source.camera.thumbnail.urlLarge,
                ),
                source.camera.search,
                source.camera.sdStreamUrl,
                source.camera.hdStreamUrl,
                source.camera.isAlive,
                checkIfFavorite(source.camera),
                source.camera.tags.split(","),
                source.camera.createdAt
            ),
            source.tags.map { tag ->
                ScTag(
                    tag.uuid,
                    tag.id,
                    tag.name,
                    tag.iconUrl,
                    tag.hash,
                    tag.sort,
                    tag.createdAt
                )
            }
        )
    }
}