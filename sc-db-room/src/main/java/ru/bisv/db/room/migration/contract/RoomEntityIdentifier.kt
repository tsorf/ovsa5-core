package ru.bisv.db.room.migration.contract

interface RoomEntityIdentifier<T> {

    val uuid: String
    val id: T
}