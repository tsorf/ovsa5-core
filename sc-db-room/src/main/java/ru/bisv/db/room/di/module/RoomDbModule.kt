package ru.bisv.db.room.di.module

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import ru.bisv.db.domain.CamerasRepo
import ru.bisv.db.domain.TagsRepo
import ru.bisv.db.room.database.DefaultScDatabase
import ru.bisv.db.room.domain.camera.repo.RoomCamerasRepo
import ru.bisv.db.room.domain.camera.repo.RoomTagsRepo
import javax.inject.Singleton

@Module
class RoomDbModule(
    private val context: Context,
) {

    private val db: DefaultScDatabase

    init {
        db = Room.databaseBuilder(
            context, DefaultScDatabase::class.java, "sc"
        ).build()
    }

    @Provides
    @Singleton
    fun provideDatabase(): RoomDatabase {
        return db
    }

    @Provides
    @Singleton
    fun provideCamerasRepo(): CamerasRepo {
        return RoomCamerasRepo(db, db.cameraDao(), db.tagDao())
    }

    @Provides
    @Singleton
    fun provideTagsRepo(): TagsRepo {
        return RoomTagsRepo(db, db.tagDao())
    }

}