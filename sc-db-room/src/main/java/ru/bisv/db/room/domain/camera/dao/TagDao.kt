package ru.bisv.db.room.domain.camera.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ru.bisv.db.room.domain.camera.model.RoomTag
import ru.bisv.db.room.domain.camera.model.RoomTagWithCameras

@Dao
internal interface TagDao {

    @Query("SELECT * FROM tag")
    fun findAll(): List<RoomTag>

    @Query("DELETE FROM tag")
    fun deleteAll()

    @Insert
    fun insertAll(tags: List<RoomTag>)

    @Query("SELECT * FROM tag WHERE uuid = :tagUuid")
    fun findTagWithCameras(tagUuid: String): RoomTagWithCameras?
}