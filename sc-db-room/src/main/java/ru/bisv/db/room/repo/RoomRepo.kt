package ru.bisv.db.room.repo

import androidx.room.RoomDatabase
import ru.bisv.db.repo.ScRepo
import java.lang.IllegalStateException

abstract class RoomRepo(
    private val database: RoomDatabase
) : ScRepo {

    override fun db(): ScRepo.Database {
        return DatabaseHolder(database)
    }

    class DatabaseHolder(
        val database: RoomDatabase
    ) : ScRepo.Database {

        override fun <Result> transaction(actions: (ScRepo.Database) -> Result): Result {
            var result: Result? = null
            if (database.inTransaction()) {
                result = actions.invoke(this)
            } else {
                database.runInTransaction {
                    result = actions.invoke(this)
                }
            }
            return result ?: throw IllegalStateException()

        }
    }
}