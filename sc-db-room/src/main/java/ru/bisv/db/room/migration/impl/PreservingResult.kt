package ru.bisv.db.room.migration.impl

import ru.bisv.db.room.dao.RoomEntity


data class PreservingResult<T : RoomEntity>(
    val updated: List<T>,
    val fresh: List<T>,
)
