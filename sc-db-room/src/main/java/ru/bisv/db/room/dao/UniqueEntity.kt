package ru.bisv.db.room.dao

interface UniqueEntity<T> : RoomEntity {

    fun ownIdentifier(): T
}