package ru.bisv.db.room.dao

import java.util.*

interface RoomEntity {

    /**
     * Должен быть аннотирован как @PrimaryKey.
     * Локальный идентификатор сущности, уникальный для текущей инсталляции.
     */
    val uuid: String

    val createdAt: Long

    fun createdAtDate(): Date {
        return Date(createdAt)
    }
}

fun generateUuid(): String {
    return UUID.randomUUID().toString()
}

fun timeStamp(): Long {
    return System.currentTimeMillis()
}