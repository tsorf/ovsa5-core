package ru.bisv.db.room.domain.camera.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ru.bisv.db.room.dao.RoomEntity
import ru.bisv.db.room.database.ScDbSchema

@Entity(
    tableName = ScDbSchema.Tables.FAVORITE,
    indices = [
        Index("stream_id", unique = true),
    ]
)
internal data class RoomCameraFavorite(
    @PrimaryKey override val uuid: String,
    @ColumnInfo(name = "stream_id") val streamId: String,
    @ColumnInfo(name = "created_at") override val createdAt: Long,
) : RoomEntity