package ru.bisv.db.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.bisv.db.room.domain.camera.dao.CameraDao
import ru.bisv.db.room.domain.camera.dao.TagDao
import ru.bisv.db.room.domain.camera.model.RoomCamera
import ru.bisv.db.room.domain.camera.model.RoomCameraAndTag
import ru.bisv.db.room.domain.camera.model.RoomCameraFavorite
import ru.bisv.db.room.domain.camera.model.RoomTag

@Database(
    entities = [
        RoomCamera::class,
        RoomTag::class,
        RoomCameraAndTag::class,
        RoomCameraFavorite::class,
    ],
    version = 1
)
internal abstract class DefaultScDatabase : RoomDatabase() {

    abstract fun cameraDao(): CameraDao

    abstract fun tagDao(): TagDao
}