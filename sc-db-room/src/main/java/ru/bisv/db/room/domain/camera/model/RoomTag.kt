package ru.bisv.db.room.domain.camera.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ru.bisv.db.room.dao.UniqueEntity
import ru.bisv.db.room.database.ScDbSchema

@Entity(
    tableName = ScDbSchema.Tables.TAG,
    indices = [
        Index("id", unique = true),
        Index("name", unique = false),
    ]
)
data class RoomTag(
    @PrimaryKey override val uuid: String,
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "icon_url") val iconUrl: String,
    @ColumnInfo(name = "hash") val hash: String,
    @ColumnInfo(name = "sort") val sort: Int,
    @ColumnInfo(name = "created_at") override val createdAt: Long,
) : UniqueEntity<String> {

    override fun ownIdentifier(): String {
        return id
    }

}