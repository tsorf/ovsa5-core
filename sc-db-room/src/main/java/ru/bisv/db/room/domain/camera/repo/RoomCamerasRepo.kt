package ru.bisv.db.room.domain.camera.repo

import androidx.room.RoomDatabase
import ru.bisv.api.model.*
import ru.bisv.db.domain.CamerasRepo
import ru.bisv.db.room.domain.camera.dao.CameraDao
import ru.bisv.db.room.domain.camera.mapper.ApiRoomCameraMapper
import ru.bisv.db.room.domain.camera.mapper.ApiRoomCameraWithTagsMapper
import ru.bisv.db.room.domain.camera.mapper.RoomCameraMapper
import ru.bisv.db.room.domain.camera.dao.TagDao
import ru.bisv.db.room.domain.camera.mapper.RoomTagMapper
import ru.bisv.db.room.domain.camera.model.RoomCameraAndTag
import ru.bisv.db.room.domain.camera.model.RoomTag
import ru.bisv.db.room.repo.RoomRepo

internal class RoomCamerasRepo(
    database: RoomDatabase,
    private val dao: CameraDao,
    private val tagDao: TagDao
) : RoomRepo(database), CamerasRepo {

    private val roomMapper = RoomCameraMapper()
    private val roomTagMapper = RoomTagMapper()


    override fun findByStreamId(streamId: String): ScCameraWithTags? {
        return db().transaction {
            val favorites = dao.findFavoriteCamerasStreamIds()
            val mapper = ApiRoomCameraWithTagsMapper {
                favorites.contains(it.streamId)
            }
            dao.findByStreamId(streamId)?.let {
                mapper.convert(it)
            }
        }
    }

    override fun checkIfLoaded(): Boolean {
        return dao.countCameras() != 0
    }

    override fun findAll(): List<ScCamera> {
        return db().transaction {
            val favorites = dao.findFavoriteCamerasStreamIds()
            val mapper = ApiRoomCameraMapper {
                favorites.contains(it.streamId)
            }
            dao.findAll().map {
                mapper.convert(it)
            }
        }
    }

    override fun findFavorites(): List<ScCamera> {
        return findAll()
    }

    override fun findByUuid(cameraUuid: String): ScCameraWithTags? {
        var result: ScCameraWithTags? = null
        db().transaction {
            val favorites = dao.findFavoriteCamerasStreamIds()
            val mapper = ApiRoomCameraWithTagsMapper {
                favorites.contains(it.streamId)
            }
            result = dao.findByUuid(cameraUuid)?.let {
                mapper.convert(it)
            }
        }
        return result
    }

    override fun updateCamerasAndTags(data: ScCamerasData) {
        val cameras = data.cameras
        val tags = data.tags
        return db().transaction {
            dao.deleteAll()
            tagDao.deleteAll()
            val preparedTags = tags.map {
                roomTagMapper.convert(it)
            }
            val preparedCameras = cameras.map {
                val tagNames = preparedTags.asSequence().filter { tag ->
                    it.camera.tags.contains(tag.id)
                }.map { tag ->
                    tag.name
                }.toList()
                val copy = it.camera.copy(
                    tags = tagNames
                )
                roomMapper.convert(copy)
            }
            dao.insertAll(preparedCameras)
            tagDao.insertAll(preparedTags)
            val findTag: (String) -> RoomTag? = { tagId ->
                preparedTags.firstOrNull {
                    it.id == tagId
                }
            }
            val connections = mutableListOf<RoomCameraAndTag>().apply {
                cameras.forEach { item ->
                    item.ids.forEach { id ->
                        val tag = findTag(id)
                        if (tag != null) {
                            add(RoomCameraAndTag(tag.uuid, item.camera.uuid))
                        }
                    }
                }
            }
            dao.connectCamerasAndTags(connections)
        }
    }

    override fun findByTag(tagUuid: String): List<ScCamera> {
        return db().transaction {
            val favorites = dao.findFavoriteCamerasStreamIds()
            val mapper = ApiRoomCameraMapper {
                favorites.contains(it.streamId)
            }
            tagDao.findTagWithCameras(tagUuid)?.cameras?.map {
                mapper.convert(it)
            } ?: listOf()
        }
    }

    override fun update(camera: ScCamera) {
        dao.update(roomMapper.convert(camera))
    }

    override fun delete(cameraUuid: String) {
        dao.deleteByUuid(cameraUuid)
    }

    override fun search(query: String): List<ScCameraWithTags> {
        TODO()
    }

}
