package ru.bisv.db.room.domain.camera.mapper

import ru.bisv.api.model.ScCamera
import ru.bisv.db.room.domain.camera.model.RoomCamera
import ru.bisv.db.room.mapper.Mapper
import javax.inject.Inject

internal class ApiRoomCameraMapper @Inject constructor(
    private val checkIfFavorite: (RoomCamera) -> Boolean
) : Mapper<RoomCamera, ScCamera> {
    override fun convert(source: RoomCamera): ScCamera {
        return ScCamera(
            source.uuid,
            source.streamId,
            source.text,
            source.name,
            source.addedAt,
            ScCamera.Thumbnail(
                source.thumbnail.url,
                source.thumbnail.urlLarge,
            ),
            source.search,
            source.sdStreamUrl,
            source.hdStreamUrl,
            source.isAlive,
            checkIfFavorite(source),
            source.tags.split(","),
            source.createdAt
        )
    }
}