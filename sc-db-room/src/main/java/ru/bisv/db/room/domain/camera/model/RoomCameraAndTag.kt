package ru.bisv.db.room.domain.camera.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import ru.bisv.db.room.database.ScDbSchema

@Entity(
    tableName = ScDbSchema.Tables.CAMERA_AND_TAG,
    primaryKeys = [
        "tag_uuid",
        "camera_uuid"
    ],
    foreignKeys = [
        ForeignKey(
            entity = RoomTag::class,
            parentColumns = ["uuid"],
            childColumns = ["tag_uuid"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = RoomCamera::class,
            parentColumns = ["uuid"],
            childColumns = ["camera_uuid"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        )
    ]
)
data class RoomCameraAndTag(
    @ColumnInfo(name = "tag_uuid") val tagUuid: String,
    @ColumnInfo(name = "camera_uuid") val cameraUuid: String,
)