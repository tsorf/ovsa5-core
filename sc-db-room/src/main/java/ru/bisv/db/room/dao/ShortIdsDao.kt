package ru.bisv.db.room.dao

interface ShortIdsDao<Id> {

    fun findAllIdentifiers(): List<Id>
}