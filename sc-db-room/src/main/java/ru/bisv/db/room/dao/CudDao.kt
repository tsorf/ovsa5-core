package ru.bisv.db.room.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface CudDao<T : RoomEntity> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entity: T): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(entities: Collection<T>): List<Long>

    @Update
    fun update(entity: T): Int

    @Update
    fun updateAll(entities: Collection<T>): Int

    @Delete
    fun delete(entity: T): Int

    @Delete
    fun deleteAll(entities: Collection<T>): Int
}