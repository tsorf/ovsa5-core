package ru.bisv.db.room.migration

import ru.bisv.db.room.migration.contract.RoomEntityIdentifier
import ru.bisv.db.repo.ScRepo
import ru.bisv.db.room.dao.CudDao
import ru.bisv.db.room.dao.ShortIdsDao
import ru.bisv.db.room.dao.UniqueEntity
import ru.bisv.db.room.migration.impl.PreservingResult

fun <Id, Entity : UniqueEntity<Id>> ScRepo.preserveUuids(
    entities: List<Entity>,
    identifiers: List<RoomEntityIdentifier<Id>>,
    copier: (Entity, RoomEntityIdentifier<Id>) -> Entity
): PreservingResult<Entity> {
    val ids = arrayListOf<Id>()
    val uuids = arrayListOf<String>()
    val copiedIdentifiers = ArrayList(identifiers)
    for (identifier in copiedIdentifiers) {
        ids.add(identifier.id)
        uuids.add(identifier.uuid)
    }

    val fresh = mutableListOf<Entity>()
    val updated = mutableListOf<Entity>()

    for (entity in entities) {
        val index = ids.indexOf(entity.ownIdentifier())
        if (index != -1) {
            val id = copiedIdentifiers[index]
            updated.add(copier(entity, id))
        } else {
            fresh.add(entity)
        }
    }
    return PreservingResult(updated, fresh)
}

/**
 * Выполняет полное сканирование, поэтому не годится
 * для больших датасетов.
 */
fun <Dao, Entity, Id, IdType> ScRepo.insertAllPreservingUuids(
    dao: Dao,
    entities: List<Entity>,
    copier: (Entity, RoomEntityIdentifier<IdType>) -> Entity
) where Dao : ShortIdsDao<Id>,
        Dao : CudDao<Entity>,
        Id : RoomEntityIdentifier<IdType>,
        Entity : UniqueEntity<IdType> {
    db().transaction {
        val ids = dao.findAllIdentifiers()
        val (updated, fresh) = preserveUuids(entities, ids, copier)
        dao.insertAll(fresh)
        dao.updateAll(updated)
    }
}