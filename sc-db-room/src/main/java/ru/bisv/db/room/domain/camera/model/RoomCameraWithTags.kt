package ru.bisv.db.room.domain.camera.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

internal data class RoomCameraWithTags(
    @Embedded
    val camera: RoomCamera,
    @Relation(
        parentColumn = "stream_id",
        entity = RoomTag::class,
        entityColumn = "uuid",
        associateBy = Junction(
            value = RoomCameraAndTag::class,
            parentColumn = "camera_uuid",
            entityColumn = "tag_uuid"
        )
    )
    val tags: List<RoomTag>
)