package ru.bisv.db.room.domain.camera.mapper

import ru.bisv.api.model.ScTag
import ru.bisv.db.room.domain.camera.model.RoomTag
import ru.bisv.db.room.mapper.Mapper
import javax.inject.Inject

internal class RoomTagMapper @Inject constructor(

) : Mapper<ScTag, RoomTag> {
    override fun convert(source: ScTag): RoomTag {
        return RoomTag(
            source.uuid,
            source.id,
            source.name,
            source.iconUrl,
            source.hash,
            source.sort,
            source.createdAt
        )
    }
}