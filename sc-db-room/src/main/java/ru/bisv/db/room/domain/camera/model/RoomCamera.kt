package ru.bisv.db.room.domain.camera.model

import androidx.room.*
import ru.bisv.db.room.dao.RoomEntity
import ru.bisv.db.room.dao.UniqueEntity
import ru.bisv.db.room.database.ScDbSchema

@Entity(
    tableName = ScDbSchema.Tables.CAMERA,
    indices = [
        Index("stream_id", unique = true),
        Index("is_alive", unique = false),
        Index("search", unique = false),
        Index("text", unique = false),
    ]
)
internal data class RoomCamera(
    @PrimaryKey
    override val uuid: String,
    @ColumnInfo(name = "stream_id") val streamId: String,
    @ColumnInfo(name = "text") val text: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "added_at") val addedAt: String,
    @Embedded val thumbnail: RoomCameraThumbnail,
    @ColumnInfo(name = "search") val search: String?,
    @ColumnInfo(name = "sd_stream_url") val sdStreamUrl: String?,
    @ColumnInfo(name = "hd_stream_url") val hdStreamUrl: String?,
    @ColumnInfo(name = "is_alive") val isAlive: Boolean,
    @ColumnInfo(name = "tags_string") val tags: String,
    @ColumnInfo(name = "created_at") override val createdAt: Long
) : RoomEntity {


    data class RoomCameraThumbnail(
        @ColumnInfo(name = "url") val url: String,
        @ColumnInfo(name = "url_large") val urlLarge: String
    )
}