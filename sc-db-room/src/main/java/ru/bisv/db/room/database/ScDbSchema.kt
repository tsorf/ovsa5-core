package ru.bisv.db.room.database

object ScDbSchema {

    object Tables {

        const val CAMERA = "camera"
        const val TAG = "tag"
        const val CAMERA_AND_TAG = "camera_tag"
        const val FAVORITE = "favorite"

    }
}