package ru.bisv.db.room.domain.camera.mapper

import ru.bisv.api.model.ScTag
import ru.bisv.db.room.domain.camera.model.RoomTag
import ru.bisv.db.room.mapper.Mapper
import javax.inject.Inject

internal class ApiRoomTagMapper @Inject constructor(

) : Mapper<RoomTag, ScTag> {
    override fun convert(source: RoomTag): ScTag {
        return ScTag(
            source.uuid,
            source.id,
            source.name,
            source.iconUrl,
            source.hash,
            source.sort,
            source.createdAt
        )
    }
}