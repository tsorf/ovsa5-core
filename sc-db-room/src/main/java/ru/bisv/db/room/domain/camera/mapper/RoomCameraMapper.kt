package ru.bisv.db.room.domain.camera.mapper

import ru.bisv.api.model.ScCamera
import ru.bisv.db.room.domain.camera.model.RoomCamera
import ru.bisv.db.room.mapper.Mapper
import javax.inject.Inject

internal class RoomCameraMapper @Inject constructor(

) : Mapper<ScCamera, RoomCamera> {
    override fun convert(source: ScCamera): RoomCamera {
        return RoomCamera(
            source.uuid,
            source.streamId,
            source.text,
            source.name,
            source.addedAt,
            RoomCamera.RoomCameraThumbnail(
                source.thumbnail.url,
                source.thumbnail.urlLarge,
            ),
            source.search,
            source.sdStreamUrl,
            source.hdStreamUrl,
            source.isAlive,
            source.tags.joinToString(",") { it },
            source.createdAt
        )
    }
}